python-getting-started
A barebones Python app, which can easily be deployed to Heroku.
This application support the Getting Started with Python on Heroku article - check it out.

Running Locally
Make sure you have Python installed properly.  Also, install the Heroku Toolbelt and Postgres.
$ git clone git@github.com:heroku/python-getting-started.git
$ cd python-getting-started
$ pip install -r requirements.txt
$ createdb python_getting_started
$ foreman run python manage.py migrate
$ python manage.py collectstatic
$ foreman start web
Your app should now be running on localhost:5000.

Deploying to Heroku
$ heroku create
$ git push heroku master
$ heroku open

Documentation
For more information about using Python on Heroku, see these Dev Center articles:

Python on Heroku
